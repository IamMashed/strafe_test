# strafe_test

Create Twitch account
Visit https://twitchapps.com/tmi/ to get your auth token

Create .env with content from env.example but with your own credentials.
Specify channel for testing purposes.
Make sure .env located in the same directory as env.example file.

Create virtual environment using python 3.7
Activate Virtual Environment

$ pip install -r requirements.txt

Create all database tables
$ export FLASK_APP=strafe.py
$ flask create-tables

Fire up the app
$ flask run

To connect to a channel
Navigate to http://localhost:5000/connect_to_channel/<channel_you_wish_to_monitor>
but make sure the channel is live by checking it at twitch.tv
Socket will be opened for 2 minutes. Received messages will be persisted into database.
After you connected to channel you can visit GraphQL endpoint and query information

GraphQL endpoint: http://127.0.0.1:5000/graphql
sample query:
query {
  perMinute,perSecond,mood
}
sample output:
{
  "data": {
    "perMinute": 100, // number of messages per minute
    "perSecond": 12, // number of messages per second
    "mood": 0 // out of 100...counting messages that contain 'Kappa'
  }
}

To run tests:
$ python -m unittest