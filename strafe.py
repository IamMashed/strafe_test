import sqlite3

from app import create_app

app = create_app('default')


def _create_tables(database):
    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute('''CREATE TABLE channel_performance
                 (id int, per_second float, per_minute float, mood text)''')
    c.execute(f"INSERT INTO channel_performance VALUES (1, null, null, null)")

    c.execute('''CREATE TABLE message
                     (username text, channel text, said text)''')

    conn.commit()
    conn.close()


def _drop_tables(database):
    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute('''DROP TABLE channel_performance''')
    c.execute('''DROP TABLE message''')
    conn.commit()
    conn.close()


@app.cli.command()
def create_tables():
    _create_tables(app.config['DB'])
    return None


@app.cli.command()
def drop_tables():
    _drop_tables(app.config['DB'])
    return None
