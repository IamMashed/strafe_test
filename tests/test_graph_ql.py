from snapshottest import TestCase
from app import create_app
from strafe import _create_tables, _drop_tables


class GraphQLTestCase(TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        _create_tables(self.app.config['DB'])

    def tearDown(self):
        _drop_tables(self.app.config['DB'])
        self.app_context.pop()

    def test_api_me(self):
        response = self.client.get('/graphql')
        self.assertMatchSnapshot(response)
