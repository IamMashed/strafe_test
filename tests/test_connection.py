import unittest
from flask import current_app
from app import create_app


class ConnectionTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])

    def test_connection(self):
        sock = self.app.socket.socket()

        sock.connect((self.app.config['TWITCH_SERVER'],
                      self.app.config['TWITCH_PORT']))

        sock.send(f"PASS {self.app.config['TWITCH_TOKEN']}\n".encode('utf-8'))
        sock.send(f"NICK {self.app.config['TWITCH_NICKNAME']}\n".encode('utf-8'))

        resp = sock.recv(2048).decode('utf-8')
        self.assertTrue(':Welcome, GLHF!' in resp)
        sock.close()
