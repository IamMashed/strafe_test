import sqlite3
import unittest
from app import create_app
from strafe import _create_tables, _drop_tables


class MessagesTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        _create_tables(self.app.config['DB'])

    def tearDown(self):
        _drop_tables(self.app.config['DB'])
        self.app_context.pop()

    def test_messages(self):
        channel = self.app.config['CHANNEL']
        response = self.client.get(f'/connect_to_channel/{channel}')
        self.assertEqual(response.status_code, 200)

        conn = sqlite3.connect(self.app.config['DB'])
        c = conn.cursor()
        c.execute('''SELECT * FROM channel_performance''')
        r = c.fetchone()
        self.assertIsNotNone(r[1])
