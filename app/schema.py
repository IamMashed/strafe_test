import sqlite3

from flask import current_app
from graphene import ObjectType, Int, Schema, relay, Field


class MessageObject(ObjectType):
    per_minute = Int()
    per_second = Int()

    def resolve_per_second(self, info):
        conn = sqlite3.connect(current_app.config['DB'])
        c = conn.cursor()
        c.execute('''SELECT * FROM channel_performance''')
        r = c.fetchone()
        conn.close()
        return r[1]

    def resolve_per_minute(self, info):
        conn = sqlite3.connect(current_app.config['DB'])
        c = conn.cursor()
        c.execute('''SELECT * FROM channel_performance''')
        r = c.fetchone()
        conn.close()
        return r[2]


class MoodObject(ObjectType):
    mood = Int()

    def resolve_mood(self, info):
        conn = sqlite3.connect(current_app.config['DB'])
        c = conn.cursor()
        c.execute('''SELECT count(*) FROM message''')
        total = c.fetchone()[0] or 1
        c.execute('''select count(*) from message where said like '%Kappa%'
        ''')
        kappa_amount = c.fetchone()[0]
        conn.close()
        return 100 * kappa_amount // total


class Query(MessageObject, MoodObject, ObjectType):
    # node = relay.Node.Field()
    message_performance = Field(MessageObject)
    chat_mood = Field(MoodObject)


schema = Schema(query=Query)
