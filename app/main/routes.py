import sqlite3
import time
import re
from emoji import demojize
from flask import current_app
from app.main import bp


@bp.route('/connect_to_channel/<string:channel_name>')
def index(channel_name):
    sock = current_app.socket.socket()

    sock.connect((current_app.config['TWITCH_SERVER'],
                  current_app.config['TWITCH_PORT']))

    sock.send(f"PASS {current_app.config['TWITCH_TOKEN']}\n".encode('utf-8'))
    sock.send(f"NICK {current_app.config['TWITCH_NICKNAME']}\n".encode('utf-8'))
    sock.send(f"JOIN #{channel_name}\n".encode('utf-8'))

    count = 0
    t = time.time()
    minute_timer = t
    start_time = t

    # tracking messages per minute differently
    messages_per_minute = 0

    conn = sqlite3.connect(current_app.config['DB'])
    c = conn.cursor()

    while True:
        resp = sock.recv(2048).decode('utf-8')

        if resp.startswith('PING'):
            sock.send("PONG\n".encode('utf-8'))
        elif len(resp) > 0:
            count += 1
            reg = re.search(
                r':(.*)\!.*@.*\.tmi\.twitch\.tv PRIVMSG #(.*) :(.*)', demojize(resp))
            if reg:
                # store the messages to database
                username, channel, said = reg.groups()
                print(username, channel, said)
                c.execute(f"INSERT INTO message VALUES ('{username}','{channel}','{said}')")
                conn.commit()

        # track messages_per_second. check after each 5 messages elapsed time.
        # per second message count = 5 / elapsed_time
        if not count % 5:
            elapsed_time = time.time() - t
            t = time.time()
            c.execute(f"UPDATE channel_performance SET per_second = {5 / elapsed_time} WHERE id = 1")
            conn.commit()

        # track messages_per_minute
        if time.time() - minute_timer > 60:
            minute_timer = time.time()
            messages_per_minute = count - messages_per_minute
            c.execute(f"UPDATE channel_performance SET per_minute = {messages_per_minute} WHERE id = 1")

        # keep channel open for time defined in app config
        print(count, time.time() - start_time)
        if time.time() - start_time > current_app.config['KEEP_CHANNEL_OPEN']:
            break

    sock.close()
    conn.close()
    return 'time elapsed and now channel is closed'
