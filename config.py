import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    TWITCH_SERVER = 'irc.chat.twitch.tv'
    TWITCH_PORT = 6667
    TWITCH_NICKNAME = os.environ.get('TWITCH_NICKNAME')
    TWITCH_TOKEN = os.environ.get('TWITCH_TOKEN')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    KEEP_CHANNEL_OPEN = 120
    DB = os.path.join(basedir, 'dev.sqlite')


class TestingConfig(Config):
    TESTING = True
    KEEP_CHANNEL_OPEN = 20
    DB = os.path.join(basedir, 'test_dev.sqlite')
    CHANNEL = os.environ.get('CHANNEL')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,

    'default': DevelopmentConfig
}
